.PHONY : dev build get_theme update_theme new

HUGO = hugo
GIT = git

HOST = 124.70.186.187
PORT = 3388
POST = 

dev :
	$(HUGO) server -D --port $(PORT) --bind 0.0.0.0 --baseURL http://$(HOST):$(PORT)/

build : update_theme
	$(HUGO) --buildDrafts --gc --verbose --minify

update_theme : get_theme
	$(GIT) submodule update --remote --merge

get_theme :
	$(GIT) submodule update --init --recursive

new :
ifneq "$(POST)" ""
	$(HUGO) new posts/$(POST).md
else
	@echo "usage: make new POST=post_name"
endif
