---
title: "记一次 MySQL 启动导致的事故"
date: 2020-12-13T01:21:10+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["MySQL"]
categories: ["DevOps"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

`MySQL` 启动存在端口被监听, 但服务没完全启动的情况.

<!--more-->

## 背景

`MySQL` 启动时会进行 `第一次` 初始化数据库, 等待 `MySQL` 完全启动后, 再进行 `第二次` 初始化数据库.

`第一次` 初始化数据库使用 `--skip-networking` (这个选项表示禁止 `MySQL` 的 `TCP/IP` 连接方式) 启动 `MySQL` 进行初始化, 初始化完成后会关闭 `--skip-networking` 选项重新启动 `MySQL`.

`第二次` 初始化数据库会设置 `root` 密码.

## 判断 `MySQL` 已完全启动的方式

尝试主动连接 `MySQL`, 连接成功则表明服务已完全启动

```bash
mysql -hlocalhost -P3306 -uroot
```

## 事故

由于上面的判断方式使用的是 `socket` 进行连接数据库, 但第一次只是禁止 `MySQL` 的 `TCP/IP` 连接方式,
所以没等 `第一次` 初始化数据库完成可能就已经进行了 `第二次` 初始化数据库,
而 `第二次` 初始化数据库提前于 `第一次` 初始化数据库设置 `root` 密码, 导致 `第一次` 初始化连不上数据库而失败,
最后没有开启 `TCP/IP` 连接方式, 应用无法连接数据库.

## 第一次修改

使用 `-h127.0.0.1` 进行连接数据库

```bash
mysql -h127.0.01 -P3306 -uroot
```

但由于 `root` 用户的 `host` 设置的是 `localhost`, 不允许 `127.0.0.1` 进行连接, 第一次修改失败.

## 第二次修改

```bash
wait-for-it.sh 127.0.0.1:3306 --timeout=300

mysql -hlocalhost -P3306 -uroot
```

[wait-for-it.sh](https://github.com/vishnubob/wait-for-it) 用于检测 `127.0.0.1:3306` 是否处于监听状态, 如果是, 则表明 `第一次` 初始化数据库完成了,
然后再使用 `localhost` 去尝试连接数据库.

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/wait-mysql-up/](https://goworker.cn/posts/wait-mysql-up/)
