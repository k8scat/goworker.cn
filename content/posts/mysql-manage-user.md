---
title: "[转载] MySQL 用户管理 - 添加用户、授权、删除用户"
date: 2020-12-27T20:05:50+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["MySQL", "user"]
categories: ["数据库"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

不要直接使用 `root` 用户管理应用数据

<!--more-->

## 添加用户

以root用户登录数据库，运行以下命令:

```mysql
create user zhangsan identified by 'zhangsan';
```

上面的命令创建了用户 `zhangsan`, 密码是 `zhangsan`. 在 `mysql.user` 表里可以查看到新增用户的信息:

```mysql
select User, Host, Password from mysql.user where User = 'zhangsan';
```

## 授权

命令格式: `grant privilegesCode on dbName.tableName to username@host identified by "password";`

```mysql
grant all privileges on zhangsanDb.* to zhangsan@'%' identified by 'zhangsan';
flush privileges;
```

上面的语句将 `zhangsanDb` 数据库的所有操作权限都授权给了用户 `zhangsan`.

在 `mysql.db` 表里可以查看到新增数据库权限的信息:

```mysql
select User, Db, Host, Select_priv, Insert_priv, Update_priv, Delete_priv from mysql.db where User = 'zhangsan';
```

也可以通过 `show grants` 命令查看权限授予执行的命令:

```mysql
show grants for 'zhangsan';
```

### `privilegesCode` 表示授予的权限类型, 常用的有以下几种类型[1]

- `all privileges`: 所有权限
- `select`: 读取权限
- `delete`: 删除权限
- `update`: 更新权限
- `create`: 创建权限
- `drop`: 删除数据库、数据表权限

### `dbName.tableName` 表示授予权限的具体库或表, 常用的有以下几种选项

- `.`: 授予该数据库服务器所有数据库的权限
- `dbName.*`: 授予dbName数据库所有表的权限
- `dbName.dbTable`: 授予数据库dbName中dbTable表的权限

### `username@host` 表示授予的用户以及允许该用户登录的IP地址. 其中Host有以下几种类型

- `localhost`: 只允许该用户在本地登录, 不能远程登录
- `%`: 允许在除本机之外的任何一台机器远程登录
- `192.168.52.32`: 具体的 `IP` 表示只允许该用户从特定IP登录.

### `password` 指定该用户登录时的密码

### `flush privileges` 表示刷新权限变更

## 修改密码

运行以下命令可以修改用户密码:

```mysql
update mysql.user set password = password('zhangsannew') where user = 'zhangsan' and host = '%';
flush privileges;
```

## 删除用户

运行以下命令可以删除用户:

```mysql
drop user zhangsan@'%';

```

`drop user` 命令会删除用户以及对应的权限, 执行命令后你会发现 `mysql.user` 表和 `mysql.db` 表的相应记录都消失了.

## 常用命令组

### 创建用户并授予指定数据库全部权限

适用于Web应用创建MySQL用户

```mysql
create user zhangsan identified by 'zhangsan';
grant all privileges on zhangsanDb.* to zhangsan@'%' identified by 'zhangsan';
flush privileges;
```

创建了用户 `zhangsan` , 并将数据库 `zhangsanDB` 的所有权限授予 `zhangsan`。如果要使 `zhangsan` 可以从本机登录，那么可以多赋予 `localhost` 权限:

```mysql
grant all privileges on zhangsanDb.* to zhangsan@'localhost' identified by 'zhangsan';
```

## 参考资料

- [1].百度.[更多关于MySQL数据库权限类型（PrivilegesCode）](http://jingyan.baidu.com/article/dca1fa6fb4ca34f1a5405270.html).[DB/OL].2013-07-13
- [2].博客园.[MySQL添加用户、删除用户与授权](http://www.cnblogs.com/fly1988happy/archive/2011/12/15/2288554.html).[DB/OL].2011-12-15

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://www.cnblogs.com/chanshuyi/p/mysql_user_mng.html](https://www.cnblogs.com/chanshuyi/p/mysql_user_mng.html)
