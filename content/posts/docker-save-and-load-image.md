---
title: "通过迁移镜像解决拉镜像太慢的问题"
date: 2021-01-07T15:18:57+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["save", "load", "migrate", "cli"]
categories: ["Docker"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://www.ruanyifeng.com/blogimg/asset/2018/bg2018020901.png"
    alt: "Docker"
    caption: "Docker"
    relative: false
comments: true
---

有时候拉取镜像特别慢，但在另一台服务器上已经有了镜像，那么就可以直接迁移镜像。

<!--more-->

## 保存镜像到压缩文件

### 命令格式

```bash
docker save [OPTIONS] IMAGE [IMAGE...]
```

### 命令说明

默认使用标准输出流生成一个压缩的镜像仓。其中包含镜像的所有父层，所有的标签以及版本，或者可以指定 `repo:tag` 进行生成。

### 使用案例

> 备份镜像

```bash
$ docker save busybox > busybox.tar

$ ls -sh busybox.tar

2.7M busybox.tar

$ docker save --output busybox.tar busybox

$ ls -sh busybox.tar

2.7M busybox.tar

$ docker save -o fedora-all.tar fedora

$ docker save -o fedora-latest.tar fedora:latest
```

> 使用 gzip 保存镜像到 `tar.gz` 压缩文件

```bash
$ docker save myimage:latest | gzip > myimage_latest.tar.gz
```

> 保存特定标签的镜像

```bash
$ docker save -o ubuntu.tar ubuntu:lucid ubuntu:saucy
```

## 通过压缩文件加载镜像

### 命令格式

```bash
docker load [OPTIONS]
```

### 命令说明

从一个文件或标准文件中加载一个镜像或镜像仓（支持 gzip，bzip2 或 xz 压缩的文件）。 它可以同时恢复镜像和标签。

### 使用案例

```bash
$ docker image ls

REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE

$ docker load < busybox.tar.gz

Loaded image: busybox:latest
$ docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
busybox             latest              769b9341d937        7 weeks ago         2.489 MB

$ docker load --input fedora.tar

Loaded image: fedora:rawhide

Loaded image: fedora:20

$ docker images

REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
busybox             latest              769b9341d937        7 weeks ago         2.489 MB
fedora              rawhide             0d20aec6529d        7 weeks ago         387 MB
fedora              20                  58394af37342        7 weeks ago         385.5 MB
fedora              heisenbug           58394af37342        7 weeks ago         385.5 MB
fedora              latest              58394af37342        7 weeks ago         385.5 MB
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/docker-save-and-load-image/](https://goworker.cn/posts/docker-save-and-load-image/)
