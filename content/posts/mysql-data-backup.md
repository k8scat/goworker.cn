---
title: "MySQL - 定时数据备份"
date: 2020-12-28T05:54:49+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["MySQL", "crontab", "mariadb", "阿里源", "备份", "CentOS", "mysqldump"]
categories: ["数据库"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

数据备份真的很重要, 因为可能有一天数据会被莫名其妙的删掉了.

<!--more-->

本文将介绍如何在 `CentOS 7` 上使用 `crontab` 和 `mysqldump` 定时备份 `mysql` 的数据.

## 更换阿里源

```bash
# 备份 repo 文件
cp /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak

# 下载
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

yum clean all # 清除缓存
yum makecache # 生成缓存
yum update
```

## 安装 `mysqldump`

```bash
yum install -y mariadb
```

## 定时任务

由于 `MySQL` 是运行在 `Docker` 容器内的, 所以本地是不能使用 `socket` 进行连接的, 需要使用参数 `-h127.0.0.1`.

```cron
# 定时任务中的 % 需要使用斜杆进行转义
0 1 * * * mysqldump -uzhangsan -ppassword -h127.0.0.1 dbName > /data/dbName_`date +'\%Y\%m\%d\%H\%M\%S'`.sql 2>&1
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/mysql-data-backup/](https://goworker.cn/posts/mysql-data-backup/)
