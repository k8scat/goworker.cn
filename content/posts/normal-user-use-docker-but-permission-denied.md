---
title: "普通用户没有权限使用 docker"
date: 2021-01-03T14:12:42+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["permission", "root"]
categories: ["Docker"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

不建议直接使用 root 用户，但普通用户使用 docker 提示权限不够。

<!--more-->

## 复现问题

> 添加用户

```bash
sudo useradd -m -s /bin/bash k8scat
```

> 执行命令

```bash
docker ps

# 输出
Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get http://%2Fvar%2Frun%2Fdocker.sock/v1.40/containers/json: dial unix /var/run/docker.sock: connect: permission denied
```

## 解决问题

> 查看 `/var/run/docker.sock` 的权限

```bash
ls -la /var/run/ | grep docker.sock

# 输出
srw-rw----  1 root    docker     0 Dec 21 16:20 docker.sock
```

> 添加用户至 docker 用户组

```bash
sudo usermod -aG docker hsowan
```

> 登录 docker 用户组

```bash
newgrp docker
```

> 再次执行命令

```bash
docker version

# 输出
Client: Docker Engine - Community
 Version:           19.03.8
 API version:       1.40
 Go version:        go1.12.17
 Git commit:        afacb8b7f0
 Built:             Wed Mar 11 01:25:46 2020
 OS/Arch:           linux/amd64
 Experimental:      false

Server: Docker Engine - Community
 Engine:
  Version:          19.03.8
  API version:      1.40 (minimum version 1.12)
  Go version:       go1.12.17
  Git commit:       afacb8b7f0
  Built:            Wed Mar 11 01:24:19 2020
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.2.13
  GitCommit:        7ad184331fa3e55e52b890ea95e65ba581ae3429
 runc:
  Version:          1.0.0-rc10
  GitCommit:        dc9208a3303feef5b3839f4323d9beb36df0a9dd
 docker-init:
  Version:          0.18.0
  GitCommit:        fec3683
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/normal-user-use-docker-but-permission-denied/](https://goworker.cn/posts/normal-user-use-docker-but-permission-denied/)
