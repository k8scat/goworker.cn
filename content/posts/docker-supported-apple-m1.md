---
title: "Docker Desktop 已经支持 Apple M1 了"
date: 2021-01-10T14:20:07+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["Apple M1", "技术预览版"]
categories: ["Docker"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/fd862cf164444529bb85fbcce63beebc~tplv-k3u1fbpfcp-zoom-1.image"
    alt: "Docker M1 Preview"
    caption: "Docker M1 Preview"
    relative: false
comments: true
---

在苹果 M1 芯片上使用的 Docker Desktop 目前是一个技术预览版，专门提供给想尝试 Docker Desktop 的实验性构建的苹果 M1 机器的早期采用者。

<!--more-->

> 注意：基于苹果 M1 芯片的 Docker Desktop 仍在开发中。建议不要在生产环境中使用技术预览版。

## 已知的问题

苹果 M1 的 Docker Desktop 的技术预览版目前有以下限制：

- 预览版不会自动更新。必须手动安装任何将来的 Docker Desktop 版本。
- 必须安装 Rosetta 2，因为某些二进制文件仍是 Darwin/AMD64。
- DNS 名称 `host.docker.internal` 仅在将 `--add-host = host.docker.internal:host-gateway` 添加到 `docker run` 命令时才有效。
- DNS 名称 `vm.docker.internal` 不起作用。
- 由于缺少 DNS 名称，Kubernetes 无法初始化。
- 未启用 HTTP 代理。
- 并非所有映像都可用于ARM64。您可以添加 `--platform linux/amd64` 在仿真下运行Intel映像。特别是，`mysql` 镜像不适用于 ARM64。但可以通过使用 `mariadb` 映像来解决此问题。
- 内核可能会出现错误。如果是这样，请在 `~/Library/Containers/com.docker.docker/Data/vms/0/console.log` 中查找要报告的 BUG 或内核错误。
- Docker 菜单中的 **Restart** 选项可能不起作用。

来源：[https://docs.docker.com/docker-for-mac/apple-m1/](https://docs.docker.com/docker-for-mac/apple-m1/)

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/docker-supported-apple-m1/](https://goworker.cn/posts/docker-supported-apple-m1/)
