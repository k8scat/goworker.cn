---
title: "使用 GitHub Actions 实现跨平台的构建发布"
date: 2021-01-04T14:46:47+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["release", "GitHub Actions", "linux", "darwin", "windows", "cross-platform"]
categories: ["GitHub"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

并不是所有的语言都能像 Golang 一样进行交叉编译的，比如使用 Python 编写的程序，
如果想要编译出不同系统下的二进制文件，必须在对应系统下进行编译才行。
但是 GitHub Actions 帮我解决了这个问题。

<!--more-->

Conba 是使用 Python 编写的一个工具，现在我们利用 GitHub Actions 进行编译成二进制文件并发布。

```yaml
name: Release Conba

on:
  push:
    tags:
    - v*

jobs:
  release:
    name: Create Release
    runs-on: ubuntu-latest
    outputs: 
      upload_url: ${{ steps.create_release.outputs.upload_url }} 
    
    steps:
      - name: Create Release
        id: create_release
        uses: actions/create-release@v1
        env:
          GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
        with:
          tag_name: ${{ github.ref }}
          release_name: ${{ github.ref }}
          draft: false
          prerelease: false

  build:
    name: Build Conba
    needs: release
    runs-on: ${{ matrix.os }}
    defaults:
      run:
        working-directory: conba
    strategy:
      matrix:
        os: [ubuntu-20.04, macos-11.0, windows-2019]
        python-version: [3.7]

    steps:
    
    - name: Set up Python ${{ matrix.python-version }}
      uses: actions/setup-python@v2
      with:
        python-version: ${{ matrix.python-version }}

    - name: Check out code into the Go module directory
      uses: actions/checkout@v2
    
    - name: Install dependencies
      run: |
        python -m pip install --upgrade pip
        pip install -r requirements.txt
        pip install pyinstaller
      
    - name: Build
      run: |
        pyinstaller -F main.py -n conba

    - name: Package
      working-directory: .
      run: |
        tar -zcvf conba-${{ matrix.os }}.tar.gz -C conba/dist *

    - name: Upload assets
      id: upload-release-asset
      uses: actions/upload-release-asset@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        upload_url: ${{ needs.release.outputs.upload_url }}
        asset_path: conba-${{ matrix.os }}.tar.gz
        asset_name: conba-${{ matrix.os }}.tar.gz
        asset_content_type: application/gzip
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/use-github-action-to-release-cross-multi-platforms/](https://goworker.cn/posts/use-github-action-to-release-cross-multi-platforms/)
