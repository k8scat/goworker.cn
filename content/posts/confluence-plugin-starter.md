---
title: "快速上手 Confluence Plugin 开发"
date: 2020-12-14T17:38:53+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["plugin"]
categories: ["Confluence"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

企业级的 `Wiki` 软件 -- `Confluence`.

<!--more-->

## [配置开发环境](https://developer.atlassian.com/server/framework/atlassian-sdk/install-the-atlassian-sdk-on-a-linux-or-mac-system/)

`CentOS7` + `VSCode`

### 安装 OpenJDK

```bash
sudo yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel

# 配置环境变量 ( vi /etc/profile )
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
```

### [VSCode 要求 Java 11](https://github.com/redhat-developer/vscode-java/wiki/JDK-Requirements#java.configuration.runtimes)

```bash
sudo yum install -y java-11-openjdk java-11-openjdk-devel
```

#### 配置 ( settings.json )

```json
"java.configuration.runtimes": [
    {
        "name": "OpenJDK-1.8.0",
        "path": "/usr/lib/jvm/java-1.8.0-openjdk",
        "default": true,
    },
    {
        "name": "OpenJDK-11",
        "path": "/usr/lib/jvm/java-11-openjdk",
    },
]
```

### 安装 `atlassian-plugin-sdk`

#### 创建源文件

```bash
sudo vi /etc/yum.repos.d/artifactory.repo
```

#### 设置源文件内容

```conf
[Artifactory]
name=Artifactory
baseurl=https://packages.atlassian.com/yum/atlassian-sdk-rpm/
enabled=1
gpgcheck=0
```

#### 开始安装

```bash
sudo yum clean all
sudo yum updateinfo metadata
sudo yum install atlassian-plugin-sdk
```

#### 校验安装

```bash
atlas-version

# 输出内容
ATLAS Version:    8.0.7
ATLAS Home:       /usr/share/atlassian-plugin-sdk-8.0.7
ATLAS Scripts:    /usr/share/atlassian-plugin-sdk-8.0.7/bin
ATLAS Maven Home: /usr/share/atlassian-plugin-sdk-8.0.7/apache-maven-3.5.4
AMPS Version:     8.0.0
--------
Executing: /usr/share/atlassian-plugin-sdk-8.0.7/apache-maven-3.5.4/bin/mvn --version -gs /usr/share/atlassian-plugin-sdk-8.0.7/apache-maven-3.5.4/conf/settings.xml
OpenJDK 64-Bit Server VM warning: ignoring option MaxPermSize=256M; support was removed in 8.0
Apache Maven 3.5.4 (1edded0938998edf8bf061f1ceb3cfdeccf443fe; 2018-06-18T02:33:14+08:00)
Maven home: /usr/share/atlassian-plugin-sdk-8.0.7/apache-maven-3.5.4
Java version: 1.8.0_272, vendor: Red Hat, Inc., runtime: /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.272.b10-1.el7_9.x86_64/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-1062.12.1.el7.x86_64", arch: "amd64", family: "unix"
```

## [启动项目](https://developer.atlassian.com/server/framework/atlassian-sdk/create-a-helloworld-plugin-project/)

### 初始化项目

```bash
atlas-create-jira-plugin

# 输入内容
Define value for groupId: : com.atlassian.tutorial
Define value for artifactId: : myPlugin
Define value for version: 1.0.0-SNAPSHOT: : 1.0.0-SNAPSHOT
Define value for package: com.atlassian.tutorial: : com.atlassian.tutorial.myPlugin

# 确认内容
Confirm properties configuration:
groupId: com.atlassian.tutorial
artifactId: myPlugin
version: 1.0.0-SNAPSHOT
package: com.atlassian.tutorial.myPlugin
Y: : Y
```

### 目录结构

```
.
├── LICENSE
├── README
├── pom.xml
└── src
    ├── main
    │   ├── java
    │   │   └── com
    │   │       └── atlassian
    │   │           └── tutorial
    │   │               └── myPlugin
    │   │                   ├── api
    │   │                   │   └── MyPluginComponent.java
    │   │                   └── impl
    │   │                       └── MyPluginComponentImpl.java
    │   └── resources
    │       ├── META-INF
    │       │   └── spring
    │       │       └── plugin-context.xml
    │       ├── atlassian-plugin.xml
    │       ├── css
    │       │   └── myPlugin.css
    │       ├── images
    │       │   ├── pluginIcon.png
    │       │   └── pluginLogo.png
    │       ├── myPlugin.properties
    │       └── js
    │           └── myPlugin.js
    └── test
        ├── java
        │   ├── it
        │   │   └── com
        │   │       └── atlassian
        │   │           └── tutorial
        │   │               └── myPlugin
        │   │                   └── MyComponentWiredTest.java
        │   └── ut
        │       └── com
        │           └── atlassian
        │               └── tutorial
        │                   └── myPlugin
        │                       └── MyComponentUnitTest.java
        └── resources
            └── atlassian-plugin.xml
```

### 运行

```bash
atlas-run
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/confluence-plugin-starter/](https://goworker.cn/posts/confluence-plugin-starter/)
