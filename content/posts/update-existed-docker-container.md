---
title: "Update Existed Docker Container"
date: 2020-12-10T03:44:29+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["Docker"]
categories: ["DevOps"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

有一个不知道怎么启动的容器, 由于某些原因会被停掉, 但又不会自动重启,
因为没有设置 `restart policy: always`.

<!--more-->

使用 `docker update` 更新已有容器的配置

```bash
docker update --restart always <container-id>
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/update-existed-docker-container/](https://goworker.cn/posts/update-existed-docker-container/)
