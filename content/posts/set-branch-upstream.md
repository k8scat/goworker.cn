---
title: "Set Branch Upstream"
date: 2020-12-10T12:16:45+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["branch", "upstream"]
categories: ["Git"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

不再使用 `git push` 设置上游分支了.

<!--more-->

在推送的时候设置上游分支

```bash
git push -u origin master
```

还可以用 `git branch` 设置本地分支的上游分支

```bash
# 当前分支是master
git branch -u origin/master

# 给本地的其他分支设置上游分支
git branch -u origin/other-branch other-branch
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/set-branch-upstream/](https://goworker.cn/posts/set-branch-upstream/)
