---
title: "CentOS 7 手动安装指定版本的 Git"
date: 2020-12-22T20:22:07+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["CentOS", "Linux", "指定版本"]
categories: ["Git"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

`yum install git` 默认安装的是比较低的版本.

<!--more-->

下载 Git 源代码: https://github.com/git/git/releases

```bash
sudo yum install -y unzip gcc openssl-devel expat-devel curl-devel

wget https://github.com/git/git/archive/v2.29.2.zip
cd git-2.29.2
make --prefix=/usr
sudo make --prefix=/usr install

```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/manual-git-install/](https://goworker.cn/posts/manual-git-install/)
