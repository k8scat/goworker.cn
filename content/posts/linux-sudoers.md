---
title: "理解 sudo 和 sudoers"
date: 2021-01-02T22:43:34+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["sudoers"]
categories: ["Linux"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: true
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

在 `Linux` 上, 只有 `root` 用户可以执行任何命令, 其他用户必须使用 `sudo` 才可执行特殊的命令.

`sudo` 是通过 `sudoers` 进行配置的.

<!--more-->

## 默认配置

`/etc/sudoers`:

```
#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL

# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d
```

## 添加配置

不建议直接修改默认配置文件, 我们可以使用 `#include` 和 `#includedir` 添加自定义的配置文件.

`/etc/sudoers.d/README`:

```
#
# As of Debian version 1.7.2p1-1, the default /etc/sudoers file created on
# installation of the package now includes the directive:
# 
#       #includedir /etc/sudoers.d
# 
# This will cause sudo to read and parse any files in the /etc/sudoers.d 
# directory that do not end in '~' or contain a '.' character.
# 
# Note that there must be at least one file in the sudoers.d directory (this
# one will do), and all files in this directory should be mode 0440.
# 
# Note also, that because sudoers contents can vary widely, no attempt is 
# made to add this directive to existing sudoers files on upgrade.  Feel free
# to add the above directive to the end of your /etc/sudoers file to enable 
# this functionality for existing installations if you wish!
#
# Finally, please note that using the visudo command is the recommended way
# to update sudoers content, since it protects against many failure modes.
# See the man page for visudo for more information.
#
```

## 理解配置

```
root    ALL=(ALL:ALL) ALL
```

上面的配置表示: `root` 用户可以在 `任意机器` 上以 `任意用户` 和 `任意用户组` 的 `任意组合` 执行 `任意命令`.

- `root`: `用户` (`%admin` 表示 `用户组`)
- `ALL=`: 所有 `host` (`sudoers` 配置可能被用到多台机器上)
- `ALL:`: 任意 `用户`
- `:ALL`: 任意 `用户组`
- `ALL`: 任意 `命令`

## 常见配置

> 允许用户使用 `sudo` 执行任意命令, 但需要输入用户密码

```
user ALL=(ALL:ALL) ALL
```

> 允许用户不需要密码使用 `sudo` 执行任意命令

```
user ALL=(ALL:ALL) NOPASSWD: ALL
```

## 参考文档

- [What is the difference between `root ALL=(ALL:ALL) ALL` and `root ALL=(ALL) ALL`?
](https://askubuntu.com/questions/546219/what-is-the-difference-between-root-all-allall-all-and-root-all-all-all)
- [Sudoers file, enable NOPASSWD for user, all commands](https://askubuntu.com/questions/334318/sudoers-file-enable-nopasswd-for-user-all-commands)
- [Sudoers Manual](https://www.sudo.ws/sudo/man/1.8.2/sudoers.man.html)

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/linux-sudoers/](https://goworker.cn/posts/linux-sudoers/)
