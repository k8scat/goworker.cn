---
title: "GitHub Actions - 使用 tag 作为发布的版本号"
date: 2020-12-20T20:43:43+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["tag", "release", "GitHub Actions"]
categories: ["GitHub"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

使用 `GitHub Actions` 发布版本时, 获取触发的 `tag` 作为发布的版本号.

<!--more-->

## 方式一

通过 `step` 获取 `tag`, 在需要使用的地方使用 `steps.get_version.outputs.VERSION`,
其中 `get_version` 是 `step` 的 `id`.

```yaml
name: Release

on:
  push:
    tags:
    - 'v*'

jobs:

  release:
    name: Release
    runs-on: ubuntu-latest
    steps:
    
    - name: Get version
      id: get_version
      run: echo ::set-output name=VERSION::${GITHUB_REF/refs\/tags\//}
    
    - name: Create Release
      uses: actions/create-release@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        tag_name: ${{ steps.get_version.outputs.VERSION }}
        release_name: ${{ steps.get_version.outputs.VERSION }}
        draft: false
        prerelease: false
```

## 方式二

直接使用 `github.ref` (触发条件为 `tag`).

```yaml
name: Release

on:
  push:
    tags:
    - 'v*'

jobs:
  release:
    name: Create Release
    runs-on: ubuntu-latest
    steps:
    
    - name: Create Release
      id: create_release
      uses: actions/create-release@v1
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}
      with:
        tag_name: ${{ github.ref }}
        release_name: ${{ github.ref }}
        draft: false
        prerelease: false
```

## 总结

如果只是想获取 `tag`, 使用 `方式二` 是最简单的, 但要注意上述例子里的触发条件是 `tag`,
如果是其他触发条件, `github.ref` 可能就不是 `tag` 了.
当然 `方式一` 作为一种使用参考, 可以用在需要获取输出值的场景.

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/github-actions-get-tag/](https://goworker.cn/posts/github-actions-get-tag/)
