---
title: "深入剖析 Kubernetes"
date: 2020-12-10T19:49:47+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["Kubernetes", "极客时间"]
categories: ["资源"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

分享一个K8S的学习资源.

<!--more-->

链接: https://pan.baidu.com/s/1tECAcsyTHP5L2-TO0iDiSQ  密码: dk71

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/deep-learn-kubernetes/](https://goworker.cn/posts/deep-learn-kubernetes/)
