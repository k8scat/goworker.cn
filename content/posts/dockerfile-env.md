---
title: "Dockerfile ENV 使用指南"
date: 2021-01-24T16:11:49+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["ENV", "Dockerfile"]
categories: ["Docker"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://img-blog.csdnimg.cn/20210124161800640.png"
    alt: "docs.docker.com"
    caption: "docs.docker.com"
    relative: false
comments: true
---

当使用 Dockerfile 进行构建镜像时，有时会需要设置容器内的环境变量。

<!--more-->

`ENV` 指令的格式如下：

```Dockerfile
ENV <key>=<value> ...
```

`ENV` 指令将环境变量 `<key>` 设置为值 `<value>`。这个值将在构建阶段的所有后续指令的环境中，
也可以[被替换使用在其他指令](https://docs.docker.com/engine/reference/builder/#environment-replacement)中。
该值将被解释为其他环境变量，因此如果引号字符没有转义，它们将被删除。像命令行解析一样，引号和反斜杠可以用于在值中包含空格。

例如：

```Dockerfile
ENV MY_NAME="John Doe"
ENV MY_DOG=Rex\ The\ Dog
ENV MY_CAT=fluffy
```

`ENV` 指令允许多个 `<key>=<value> ...` 变量同时设置，下面的例子将在生成的镜像中产生相同的结果：

```Dockerfile
ENV MY_NAME="John Doe" MY_DOG=Rex\ The\ Dog \
    MY_CAT=fluffy
```

当使用生成的镜像运行容器时，使用 `ENV` 设置的环境变量将持久存在于容器内。
你可以使用 `docker inspect` 查看这些值，并使用 `docker run --env <key>=<value>` 修改它们。

环境变量持久性可能会导致意想不到的副作用。
例如，设置 `ENV DEBIAN_FRONTEND=noninteractive` 会改变 `apt-get` 的行为，并可能让使用镜像的用户感到困惑。

如果只在构建过程中需要环境变量，而不是在最终镜像中，请考虑为单个命令设置一个值：

```Dockerfile
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y ...
```

或者使用 `ARG`，它不会在最终镜像中持久存在：

```Dockerfile
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y ...
```

> 替代语法
>  
> `ENV` 指令还允许另一种语法 `ENV <key> <value>`，省略了中间的等号。例如：
>  
> ```Dockerfile
> ENV MY_VAR my-value
> ```
>  
> 这种语法不允许在一条 `ENV` 指令中设置多个环境变量，可能会造成混淆。例如，下面的代码设置了一个值为“TWO= THREE=world”的环境变量（ONE）：
>  
> ```Dockerfile
> ENV ONE TWO= THREE=world
> ```
>  
> 支持这种替代语法为了向后兼容，但由于上述原因不鼓励使用，可能会在将来的版本中删除。

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/dockerfile-env/](https://goworker.cn/posts/dockerfile-env/)
