---
title: "Docker 容器健康检查"
date: 2021-01-20T13:40:10+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["healthcheck", "Dockerfile"]
categories: ["Docker"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "https://img-blog.csdnimg.cn/20210120134741738.jpg"
    alt: "DOCKER HEALTHCHECK"
    caption: "DOCKER HEALTHCHECK"
    relative: false
comments: true
---

Docker 容器健康检查指的是在 Dockerfile 中使用 `HEALTHCHECK` 指令对容器的运行状态进行检查，
并在 `docker ps` 的 STATUS 栏显示 healthy/unhealthy。

<!--more-->

`HEALTHCHECK` 指令有两种格式：

- `HEALTHCHECK [OPTIONS] CMD command`（通过在容器内运行命令检查容器的健康状态）
- `HEALTHCHECK NONE`（禁用从基础镜像中继承任何健康检查）

`HEALTHCHECK` 指令告诉 Docker 如何测试一个容器，以检查它是否仍在工作。
这可以检测到一些情况，例如 web 服务器陷入无限循环，无法处理新的连接，即使服务器进程仍在运行。

当容器指定了健康检查时，除了正常状态外，它还具有健康状态。此状态初始为 `starting`。
只要健康检查通过，它就会恢复到 `healthy`（无论它以前处于什么状态）。在连续失败一定次数后，它就会变得 `unhealthy`。

`CMD` 之前可以出现的选项有：

- --interval=DURATION（默认：30s）
- --timeout=DURATION（默认：30s）
- --start-period=DURATION（默认：0s）
- --retries=N（默认：3）

运行状态检查首先会在容器启动后的 `interval` 秒内运行，然后在前一次检查完成后的 `interval` 秒内再次运行。

如果一次状态检查花费的时间超过 `timeout` 秒，则认为这次检查失败。

容器的运行状态检查连续失败 `retries` 次才会被视为不健康。

`start period` 为需要时间启动的容器提供初始化时间。在此期间的探测失败将不计入最大重试次数。
但是，如果在启动期间健康检查成功，则认为容器已启动，所有连续失败的情况都将计算到最大重试次数。

Dockerfile 中只能有一个 `HEALTHCHECK` 指令。如果列出多个，则只有最后一个 `HEALTHCHECK` 才会生效。

`CMD` 关键字后面的命令可以是 shell 命令（例如 `HEALTHCHECK CMD /bin/check-running`）或 exec 数组（与其他 Dockerfile 命令一样，有关详细信息，请参见 `ENTRYPOINT`）。

`command` 的退出状态表示容器的健康状态。可能的值是：

- 0：成功--容器运行良好，可以使用
- 1：不健康—-容器不能正常工作
- 2：保留--不使用此退出码

例如，每五分钟左右检查一次 web 服务器是否能在三秒内为站点的主页提供服务：

```Dockerfile
HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl -f http://localhost/ || exit 1
```

为了帮助调试失败的探测，`command` 写在 stdout 或 stderr 上的任何输出文本（UTF-8编码）都将存储在健康状态中，并且可以通过 `docker inspect` 进行查询。
这样的输出应该保持简短（目前只存储前4096个字节）。

当容器的健康状态发生变化时，将生成一个具有新状态的 `health_status` 事件。

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/docker-container-healthcheck/](https://goworker.cn/posts/docker-container-healthcheck/)
