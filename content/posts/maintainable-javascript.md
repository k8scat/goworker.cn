---
title: "编写可维护的 JavaScript (中/英文)"
date: 2020-12-14T18:47:25+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["JavaScript"]
categories: ["资源"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

分享一个高质量的 `电子书` 资源.

<!--more-->

链接: https://pan.baidu.com/s/1qCizoCu9eooEQREWDrPESA  密码: n6av

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/maintainable-javascript/](https://goworker.cn/posts/maintainable-javascript/)
