---
title: "Linux - 添加用户至用户组"
date: 2020-12-24T11:42:43+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["usermod", "adduser"]
categories: ["Linux"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

<!--more-->

## 基本命令

```bash
usermod -G group1,group2 user
```

但上面的命令会将用户踢出当前 `用户组`.

## 使用 `-a` 参数

```bash
usermod -aG group1,group2 user
```

## 其他方式

> 使用 `adduser`

```bash
adduser user group
```

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/linux-add-user-to-new-groups/](https://goworker.cn/posts/linux-add-user-to-new-groups/)
