---
title: "如何看待 国产开源软件 购买 GitHub Star"
date: 2020-12-23T13:56:13+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["国产", "开源软件", "购买", "GitHub", "Star"]
categories: ["OpenSource"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

`GitHub` ([https://github.com](https://github.com)) 是全球最大的 `男性交友网站` (`开源项目托管平台`).
一个项目的流行程度通常可以看该项目的 `Star` (关注数), `Star` 越多, 说明这个项目越受人们欢迎.
但有时候需要擦亮自己的双眼!

<!--more-->

## 私信 现金红包

今天突然看到 `CSDN` 给我发来一条私信 `CSDN 现金红包`, 下图是私信页面.

![](https://img-blog.csdnimg.cn/20201223144847767.png)

## 你点 Star, 我送豪礼

发红包肯定是要点开来看看的:

_【你点 Star，我送豪礼】旷视自主研发的工业级深度学习框架——天元 MegEngine 重磅升级！集训练推理一体、全平台高效支持、将动态训练代码转换为静态图，优化训练速度等。 为回馈开发者，只要你点 Star，我就送豪礼！感谢你为中国原创助力，凡 Star 天元 MegEngine 用户有机会获得炫酷天元 MegEngine 纪念 T 恤、CSDN定制键盘托、精美 AI 技术书籍以及100小时在线算力卡，欢迎小伙伴积极参与点赞活动。为中国原创点赞，为天元 MegEngine 点赞！_

![](https://img-blog.csdnimg.cn/20201223144848132.png)

上图页面链接: [https://marketing.csdn.net/p/717176bb3f6d2c9c36610987daf57338](https://marketing.csdn.net/p/717176bb3f6d2c9c36610987daf57338)

## 评价

一开始觉得怎么会有人花钱买 `GitHub Star` 呢, 但仔细想想, 这可能就是有钱人的玩法吧.

![](https://img-blog.csdnimg.cn/2020122314484838.png)

项目链接: [https://github.com/MegEngine/MegEngine](https://github.com/MegEngine/MegEngine)

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/china-opensource-software-buy-github-stars/](https://goworker.cn/posts/china-opensource-software-buy-github-stars/)
