---
title: "在个人电脑上使用 scp 在两台服务器之间拷贝文件"
date: 2020-12-30T16:25:25+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["scp", "-3"]
categories: ["DevOps"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

一般人都不知道的 `scp` 用法, 我就是那个一般人~

<!--more-->

## 一般用法

### 上传本地文件到远程机器指定目录

复制本地 `/opt/soft/` 目录下的文件 `nginx-0.5.38.tar.gz` 到远程机器 `10.10.10.10` 的 `opt/soft/scptest` 目录.

```bash
scp -rp -P 2222 /opt/soft/nginx-0.5.38.tar.gz root@10.10.10.10:/opt/soft/scptest
```

### 从远程机器复制文件到本地目录

从 `10.10.10.10` 机器上的 `/opt/soft/` 的目录中下载 `nginx-0.5.38.tar.gz` 文件到本地 `/opt/soft/` 目录中.

```bash
scp root@10.10.10.10:/opt/soft/nginx-0.5.38.tar.gz /opt/soft/
```

### 从远程机器复制目录到本地

从 `10.10.10.10` 机器上的 `/opt/soft/` 中下载 `mongodb` 目录到本地的 `/opt/soft/` 目录来.

```bash
scp -r root@10.10.10.10:/opt/soft/mongodb /opt/soft/
```

## 第三者用法

在个人电脑上使用 `scp` 在两台服务器之间拷贝文件

### 设置 `ssh config`

编辑 `~/.ssh/config`, 添加如下内容:

```config
Host remote1
    HostName remote1.example.org
    Port 2222
    IdentityFile /path/to/host1-id_rsa

Host remote2
    HostName remote2.example.org
    Port 6969
    IdentityFile /path/to/host2-id_rsa
```

### 开始拷贝

```bash
scp -3 user1@remote1:/home/user1/file1.txt user2@remote2:/home/user2/file1.txt
```


![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/scp-between-two-remote-hosts-from-my-third-pc/](https://goworker.cn/posts/scp-between-two-remote-hosts-from-my-third-pc/)
