---
title: "复杂的 Jenkinsfile 案例"
date: 2020-12-10T05:32:59+08:00
draft: true
weight: 1
aliases: ["/first"]
tags: ["Jenkins", "Pipeline", "Jenkinsfile"]
categories: ["CI/CD"]
author: "Noah Wan"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
disableShare: false
cover:
    image: "<image path/url>"
    alt: "<alt text>"
    caption: "<text>"
    relative: false
comments: true
---

通过 `Jenkinsfile` 可以编写 `Pipeline`, 这里分享一个使用了 `agent`, `parameters`, `script`, `build job`, `sh`, `if-else` 的复杂的 `Jenkinsfile`,
其中 `parameters` 包含 `extendedChoice`.

<!--more-->

```groovy
pipeline {
    agent { label 'master' }

    parameters {
        extendedChoice (
            name: 'projectApiBranch',
            defaultValue: 'master',
            description: 'project api 分支名',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_bin_file_branches.sh project-api demo-ai-api-core' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list project api branches'
        )
        extendedChoice (
            name: 'projectWebBranch',
            defaultValue: 'master',
            description: 'project web 分支名',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_bin_file_branches.sh project-web demo-project-web' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list project web branches'
        )
        extendedChoice (
            name: 'wikiApiBranch',
            defaultValue: 'master',
            description: 'wiki api 分支名',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_bin_file_branches.sh wiki-api demo-wiki-api-core' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list wiki api branches'
        )
        extendedChoice (
            name: 'wikiWebBranch',
            defaultValue: 'master',
            description: 'wiki web 分支名',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_bin_file_branches.sh wiki-web demo-wiki-web' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list wiki api branches'
        )
        extendedChoice (
            name: 'thirdImportTag',
            defaultValue: 'v1.0.3',
            description: '第三方导入版本',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', 'cd /var/jenkins_home/demo/go/src/github.com/bangwork/demo-third-importer && git tag -l |grep -v "{}"|sed "s/\\\\^{}//g"|sed "s/.*tags\\\\///g"|sort -rV ']
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list third import tags'
        )
        extendedChoice (
            name: 'devopsBranch',
            defaultValue: 'master',
            description: 'devops 分支名',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_bin_file_branches.sh devops-api demo-devops-api-core' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list devops branches'
        )
        extendedChoice (
            name: 'auditlogSyncTag',
            defaultValue: 'master',
            description: '审计日志组件分支号',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_bin_file_branches.sh audit-log-sync demo-ai-audit-log-sync' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list auditlog sync tags'
        )
        choice (
            description: 'fluter（移动端web页）版本',
            name: 'mobileWebTag',
            choices: ['v1.0.0', 'v1.1.0', 'v1.1.1', 'v1.2.1', 'v1.3.0', 'v1.3.1', 'v1.3.2']
        )
        choice (
            name: 'binlogSyncTag',
            description: '日志事件同步组件版本',
            choices: ['v1.0.9', 'v1.0.8', 'v1.0.7', 'v1.0.6', 'v1.0.5', 'v1.0.3', 'v1.0.0']
        )
        string (
            name: 'baseImageVersion',
            defaultValue: 'latest',
            description: '基础镜像版本号，默认值为latest，当需要更换基础镜像时，在构建机上准备好基础镜像，填写指定版本，如v1.0.8'
        )
        string (
            name: 'demobuildBinVersion',
            defaultValue: 'default',
            description: 'release镜像生成器的版本，当需要更换生成器时，在构建机上准备好二进制程序，并填写指定版本号'
        )

        extendedChoice (
            name: 'democonfigureBranch',
            defaultValue: 'master',
            description: 'demo-ai-docker 分支',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_bin_file_branches.sh demo-ai-docker democonfigure' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list demo-ai-docker branches'
        )

        string (
            name: 'instanceName',
            defaultValue: '',
            description: '不超过8个字符，不要求唯一，仅用于标识测试环境，一般与迭代号相同，例如S1001，可自由命名（如果未填，则随机生成）'
        )

        extendedChoice (
            name: 'version',
            defaultValue: '',
            description: '版本号（如果不是使用旧的版本号，此参数不生效）',
            type: 'PT_SINGLE_SELECT',
            groovyScript: '''def ver_keys = [ 'bash', '-c', '/var/jenkins_home/scripts/get_versions.sh | sort -rV' ]
ver_keys.execute().text.tokenize('\\n')''',
            multiSelectDelimiter: ',',
            visibleItemCount: 5,
            descriptionGroovyScript: 'list versions'
        )
        booleanParam(
            name: 'useOldVersion',
            defaultValue: false,
            description: '是否使用旧的版本号，如果是则跳过build_image，并使用version对应的版本号'
        )
    }

    stages {
        stage('build_image') {
            steps {
                script {
                    if (params.useOldVersion == false) {
                        build job: 'build-image', parameters: [
                            extendedChoice(name: 'projectApiBranch', value: "${params.projectApiBranch}"),
                            extendedChoice(name: 'projectWebBranch', value: "${params.projectWebBranch}"),
                            extendedChoice(name: 'wikiApiBranch', value: "${params.wikiApiBranch}"),
                            extendedChoice(name: 'wikiWebBranch', value: "${params.wikiWebBranch}"),
                            extendedChoice(name: 'thirdImportTag', value: "${params.thirdImportTag}"),
                            extendedChoice(name: 'devopsBranch', value: "${params.devopsBranch}"),
                            string(name: 'mobileWebTag', value: "${params.mobileWebTag}"),
                            string(name: 'binlogSyncTag', value: "${params.binlogSyncTag}"),
                            extendedChoice(name: 'auditlogSyncTag', value: "${params.auditlogSyncTag}"),
                            string(name: 'baseImageVersion', value: "${params.baseImageVersion}"),
                            string(name: 'demobuildBinVersion', value: "${params.demobuildBinVersion}")
                        ]
                    } else {
                        echo 'use old version'
                    }
                }
            }
        }

        stage('build_install_pak') {
            steps {
                script {
                    if (params.useOldVersion == true) {
                        version = params.version
                    } else {
                        version = sh (
                            script: 'echo 0.1.`ssh -p8022 root@172.31.115.24 cat /disk2/mars_jenkins_user_data/demo/buildNum.info`',
                            returnStdout: true
                        ).trim()
                    }
                    build job: 'build-install-pak', parameters: [
                        extendedChoice(name: 'version', value: "${version}"),
                        extendedChoice(name: 'branch', value: "${params.democonfigureBranch}")
                    ]
                }
            }
        }

        stage('create_test_env') {
            steps {
                build job: 'create-test-env', parameters: [
                    string(name: 'instanceName', value: "${params.instanceName}"),
                    extendedChoice(name: 'version', value: "${version}")
                ]
            }
        }
    }
}
```

Official Docs: https://www.jenkins.io/doc/book/pipeline/

![公众号: 源自开发者](https://img-blog.csdnimg.cn/20201226195502163.png)

原文链接：[https://goworker.cn/posts/jenkinsfile-reference/](https://goworker.cn/posts/jenkinsfile-reference/)
