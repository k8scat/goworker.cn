# [GoWorker](https://goworker.cn)

Powered by [Hugo](https://gohugo.io/)

Theme [PaperMod](https://github.com/adityatelange/hugo-PaperMod)

## 编辑工具

- [Markdown Nice](https://www.mdnice.com/)
- [Minimalist Markdown Editor](https://chrome.google.com/webstore/detail/minimalist-markdown-edito/pghodfjepegmciihfhdipmimghiakcjf)

## New post

```bash
make new POST=post_name
```

## 同步平台

- [掘金](https://juejin.cn/)
- [知乎](https://www.zhihu.com/)
- [微信公众号](https://mp.weixin.qq.com)
  - 源自开发者
- [CSDN](https://csdn.net)
- [SegmentFault](https://segmentfault.com/)
- [OSChina](https://oschina.net)
- [InfoQ](https://xie.infoq.cn/)

> 国外网站

- [Medium](https://medium.com/)